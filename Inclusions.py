""" This module provides functions to solve the flow and transport equations
    in porous media at the Darcy scale. The domain is rectangular and the
    permeability field can contain circular inclusions arranged regularly or
    randomly.

    Author: Juan J. Hidalgo, IDAEA-CSIC, Barcelona, Spain.
    Acknowledgements:
        Project MHetScale (FP7-IDEAS-ERC-617511)
            European Research Council

        Project Mec-MAT (CGL2016-80022-R)
            Spanish Ministry of Economy and Competitiveness
"""
import pickle
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as lgsp
import ipdb
################
def run_simulation(*, Lx=1., Ny=50,
                   pack='tri', n_incl_y=3, Kfactor=0.1,
                   target_incl_area=0.5,radius=None,
                   bcc='head', isPeriodic=True, integrateInTime=True,
                   tmax=10., dt=None, Npart=100,
                   plotPerm=False, plotFlow=False,
                   plotTpt=False, plotBTC=False,
                   filename=None, doPost=True):
    """ Runs a simulation."""

    grid = setup_grid(Lx, Ny)

    kperm, incl_ind, grid = permeability(grid, n_incl_y, Kfactor,
                                         target_incl_area=target_incl_area,
                                         radius=radius, isPeriodicK=isPeriodic,
                                         pack=pack, filename=filename,
                                         plotit=plotPerm, saveit=True)

    ux, uy = flow(grid, 1./kperm, bcc, isPeriodic=isPeriodic, plotHead=plotFlow)

    if dt is None:
        if integrateInTime:
            tx = grid['Lx']/grid['Nx']/ux.max()
            ty = grid['Ly']/grid['Ny']/uy.max()
            dt = np.min([tx, ty, 1e-3])
        else:
            dt = 0.1*Kfactor*grid['Lx']/grid['Nx']


    if integrateInTime:
        arrival_times, t_in_incl = transport(grid, incl_ind,
                                             Npart, ux, uy,
                                             tmax, dt, isPeriodic=isPeriodic,
                                             plotit=plotTpt, CC=kperm)
    else:
        arrival_times, t_in_incl = transport_ds(grid, incl_ind,
                                                Npart, ux, uy,
                                                dt, isPeriodic=isPeriodic)


    if filename is None:
        filename = 'K' + str(Kfactor).replace('.', '') + pack + 'Ninc' + str(n_incl_y)

    cbtc_time, cbtc = compute_cbtc(arrival_times,
                                   saveit=True, showfig=plotBTC, savefig=False,
                                   filename=filename)


    with open(filename + '.plk', 'wb') as ff:
        pickle.dump([Npart, t_in_incl, arrival_times], ff, pickle.HIGHEST_PROTOCOL)

    print("End of simulation.\n")

    if doPost:

        postprocess(Npart, t_in_incl, arrival_times, fname=filename,
                   savedata=True, savefig=False,
                   showfig=False, figformat='pdf',
                   bins='auto', dofullpostp=False)

        print("End of postprocess.\n")

    return True
################
def setup_grid(Lx, Ny):
    """Grid set up. Given the length in x (Lx) and the
       number of cells in y (Ny) returns a numpy structured
       array  with Lx, Ly, Nx, Ny. Ly is always 1 and Nx is
       computed so that the cells are squares.
    """

    if Ny is None:
        Ny = 0

    grid = np.zeros(1, dtype={'names':['Lx', 'Ly', 'Nx', 'Ny'], \
                'formats':['float64', 'float64', 'int32', 'int32']})
    grid['Lx'] = Lx
    grid['Ly'] = 1.
    grid['Nx'] = np.int(Lx*Ny)
    grid['Ny'] = Ny

    return grid
################
def unpack_grid(grid):
    return grid['Lx'][0], grid['Ly'][0], grid['Nx'][0], grid['Ny'][0]

################
def permeability(grid, n_incl_y, Kfactor=1., pack='sqr',
                 target_incl_area=0.5, radius=None, isPeriodicK=False,
                 filename=None, plotit=False, saveit=True):
    """Computes permeability parttern inside a 1. by Lx rectangle.
       The area covered by the inclusions is 1/2 of the rectanle.
       If the arrangement os random, the radius is reduced by 10%
       to increase the chance of achieving the target inclusion area.

       Then the domain is resized by adding 3*radius on the left
       to avoid boundary effects.
       The dimension of the box and the discretization are changed
       so that the grid remains regular.

       The function returns the permeability field, the cell indexes
       the location of the inclusions and the new grid.
    """

    import RecPore2D as rp

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    n_incl_x = np.int(Lx*n_incl_y)
    n_incl = number_of_grains(n_incl_y, n_incl_x, pack)

    if radius is None:
        total_area = Lx*Ly
        radius = np.sqrt((target_incl_area*total_area)/(np.pi*n_incl))

    if pack == 'sqr' or pack == 'tri':
        pore = rp.RegPore2D(nx=n_incl_x, ny=n_incl_y,
                            radius=radius, packing=pack)
        pore.isPeriodic = isPeriodicK
        if isPeriodicK:
            throat = (Ly - (2.0*n_incl_y*radius))/n_incl_y
        else:
            throat = (Ly - 2.0*n_incl_y*radius)/(n_incl_y + 1.0)

        pore.throat = throat
        pore.bounding_box = ([0.0, 0.0, 0.5], [Lx, Ly, 1.0])
        pore.xoffset = 0.


        #delta = Lx - n_incl_x*(throat + 2.*radius)
        #displacement = (delta - throat)/2.

    elif pack == 'rnd':

        pore = rp.RndPore2D(lx=Lx, ly=Ly,
                            rmin=0.90*radius, rmax=0.90*radius,
                            target_porosity=1.-target_incl_area,
                            packing='rnd')

        pore.ngrains_max = int(1.1*n_incl)
        pore.ntries_max = int(1e5)

    # Centers circles and resizes domain to avoid boundary effects
    # (2*radius added to the left and to the right).
    xmin = np.min(pore.circles[:]['x'])
    rr = pore.circles[0]['r']
    #Leftmost circle's border moved to x=0.
    pore.circles[:]['x'] = pore.circles[:]['x'] - xmin + rr
    #Space behind rightmost circle.
    xmax = np.max(pore.circles[:]['x'])
    displacement = Lx - (xmax + rr)
    #Circles centering
    pore.circles[:]['x'] = pore.circles[:]['x'] + 0.5*displacement
    #Adding of 4*r
    displacement = np.ceil(4.*radius)
    pore.circles[:]['x'] = pore.circles[:]['x'] + 0.5*displacement

    # if no discretization is given a 30th of the smallest inclusion's
    # radius is chosen as cell size.
    if Ny < 1:
        Ny = np.int(30/pore.circles[:]['r'].min())

    grid = setup_grid(Lx + displacement, Ny)
    kperm, incl_ind = perm_matrix(grid, pore.circles, Kfactor)

    # kperm[xx>-1] = 1.
    # kperm[yy>0.5] = 0.1
    # kperm[xx<0.25] = 1.
    # kperm[xx>0.75] = 1.
    if plotit:
        plot2D(grid, kperm, title='kperm', allowClose=True)

    if saveit:
        if filename is None:
            fname = 'perm.plk'
        else:
            fname = filename + '-perm.plk'

        with open(fname, 'wb') as ff:
            pickle.dump([grid, pore.circles, Kfactor],
                        ff, pickle.HIGHEST_PROTOCOL)

    return kperm, incl_ind, grid

################
def flow(grid, mu, bcc, isPeriodic=True, plotHead=False):
    ''' Solves the flow equation and returns the velocity
        at the cell's faces.
        mu = 1./kperm
    '''

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    dx = Lx/Nx
    dy = Ly/Ny
    dx2 = dx*dx
    dy2 = dy*dy
    Np = Nx*Ny

    # Transmisibility matrices.

    Tx = np.zeros((Ny, Nx + 1))
    #Tx[:, 1:Nx] = (2.*dy)/(mu[:, 0:Nx-1] + mu[:, 1:Nx+1])
    Tx[:, 1:Nx] = (2.*dy)/(mu[:, 0:Nx-1] + mu[:, 1:Nx]) #new
    Ty = np.zeros([Ny + 1, Nx])
    #Ty[1:Ny, :] = (2.*dx)/(mu[0:Ny-1, :] + mu[1:Ny+1, :])
    Ty[1:Ny, :] = (2.*dx)/(mu[0:Ny-1, :] + mu[1:Ny, :]) #new

    Tx1 = Tx[:, 0:Nx].reshape(Np, order='F')
    Tx2 = Tx[:, 1:Nx+1].reshape(Np, order='F')

    Ty1 = Ty[0:Ny, :].reshape(Np, order='F')
    Ty2 = Ty[1:Ny+1, :].reshape(Np, order='F')

    Ty11 = Ty1
    Ty22 = Ty2

    TxDirich = np.zeros(Ny)
    TxDirich = (2.*dy)*(1./mu[:, Nx-1])


    if bcc == 'head':
        TxDirichL = (2.*dy)*(1./mu[:, 0])

    #Assemble system of equations
    Dp = np.zeros(Np)
    Dp = Tx1/dx2 + Ty1/dy2 + Tx2/dx2 + Ty2/dy2

    #Dirichlet b.c. on the right
    Dp[Np-Ny:Np] = Ty1[Np-Ny:Np]/dy2 + Tx1[Np-Ny:Np]/dx2 + \
                   Ty2[Np-Ny:Np]/dx2 + TxDirich/dx2

    if bcc == 'head':
        Dp[0:Ny] = Ty1[0:Ny]/dy2 + Tx2[0:Ny]/dx2 + \
                   Ty2[0:Ny]/dx2 + TxDirichL/dx2

    #Periodic boundary conditions
    TypUp = np.zeros(Np)
    TypDw = np.zeros(Np)

    if isPeriodic:
        Typ = (2.*dx)/(mu[Ny - 1, :] + mu[0, :])

        TypDw[0:Np:Ny] = Typ
        TypUp[Ny-1:Np:Ny] = Typ

        Dp[0:Np:Ny] = Dp[0:Np:Ny] + Typ/dy2
        Dp[Ny-1:Np:Ny] = Dp[Ny-1:Np:Ny] + Typ/dy2

    # TO DO. Check if this saves memory
    import gc
    mu = None
    gc.collect()

    Am = sp.spdiags([-Tx2/dx2, -TypDw/dy2, -Ty22/dy2, Dp,
                     -Ty11/dy2, -TypUp/dy2, -Tx1/dx2],
                    [-Ny, -Ny + 1, -1, 0, 1, Ny - 1, Ny], Np, Np,
                    format='csr')

    #RHS - Boundary conditions
    u0 = 1.*dy
    hL = 1.
    hR = 0.
    S = np.zeros(Np)
    S[Np-Ny:Np+1] = TxDirich*(hR/dx/dx) # Dirichlet X=1;
    if bcc == 'head':
        S[0:Ny] = TxDirichL*(hL/dx/dx) # Dirichlet X=0;
    else:
        S[0:Ny] = u0/dx #Neuman BC x=0;

    try:
        head = lgsp.spsolve(Am, S).reshape(Ny, Nx, order='F')
    except:
        print(grid['Ny'])
        print(grid['Nx'])
        #import sys
        #sys.exit("spsolve out of memory.")
        print("spsolve out of memory.")
        head, info = lgsp.cg(Am, S)
        print(info)
        head = head.reshape(Ny, Nx, order='F')

    Am = None
    S = None
    gc.collect()
    #Compute velocities

    ux = np.zeros([Ny, Nx+1])
    uy = np.zeros([Ny+1, Nx])

    if bcc == 'head':
        ux[:, 0] = -TxDirichL*(head[:, 0] - hL)/dx
    else:
        ux[:, 0] = u0

    #ux[:, 1:Nx] = -Tx[:, 1:Nx]*(head[:, 1:Nx+1] - head[:, 0:Nx-1])/dx
    ux[:, 1:Nx] = -Tx[:, 1:Nx]*(head[:, 1:Nx] - head[:, 0:Nx-1])/dx
    ux[:, Nx] = -TxDirich*(hR - head[:, Nx-1])/dx

    #uy[1:Ny, :] = -Ty[1:Ny, :]*(head[1:Ny+1, :] - head[0:Ny-1, :])/dy
    uy[1:Ny, :] = -Ty[1:Ny, :]*(head[1:Ny, :] - head[0:Ny-1, :])/dy

    #periodic
    if isPeriodic:
        uy[0, :] = Typ*(head[0, :] - head[Ny - 1, :])/dy
        uy[Ny, :] = uy[0, :]


    if plotHead:
        plot2D(grid, head, title='head', allowClose=True)
        plot2D(grid, ux/dy, title='ux', allowClose=True)
        plot2D(grid, uy/dx, title='uy', allowClose=True)

    return ux/dy, uy/dx

#####
def transport(grid, incl_ind, Npart, ux, uy, tmax, dt, isPeriodic=False,
              plotit=False, CC=None):
    '''Solves the transport of a line of concentration initially at the
       left boundary using a particle tracking method.

       Returns the arrival times of the particles to the right boundary
       and data about the time spent in the inclusions.'''

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    if plotit and  CC is not None:
        figt, axt, cbt = plot2D(grid, CC)

    t = 0.
    xp = np.zeros(Npart)
    yp = np.arange(Ly/Npart/2.0, Ly, Ly/Npart)
    #qq
    #rad = 0.25231325
    #alpha = np.arange(np.pi/Npart/2.0, np.pi, np.pi/Npart)
    #alpha = np.arange(np.pi/2. + np.pi/Npart/2.0, 3.*np.pi/2., np.pi/Npart)
    #xp = 1.5 + rad*np.cos(alpha)
    #yp  = 0.5 +rad*np.sin(alpha)
    #qq
    dx = np.float(Lx/Nx)
    dy = np.float(Ly/Ny)

    Ax = ((ux[:, 1:Nx + 1] - ux[:, 0:Nx])/dx).flatten(order='F')
    Ay = ((uy[1:Ny + 1, :] - uy[0:Ny, :])/dy).flatten(order='F')

    ux = ux[:, 0:Nx].flatten(order='F')
    uy = uy[0:Ny, :].flatten(order='F')

    x1 = np.arange(0., Lx + dx, dx) #faces' coordinates
    y1 = np.arange(0., Ly + dy, dy)

    i = 0

    lint = None

    #number of inclusions
    num_incl = incl_ind.max().astype(int)


    #time of each particle in each inclusion
    # It contains nincl dictionaries
    # Each dictionary contains the particle and the time spent.
    t_in_incl = []

    for i in range(num_incl):
        t_in_incl.append({})

    nwrite = 10 # np.max([int((tmax/dt)/1000),10])
    arrival_times = np.zeros(Npart)
    i = 0

    isIn = np.where(xp < Lx)[0]
    if tmax is None:
        print('tmax not defined. tmax = 1.0')
        tmax = 1.0
    while t <= tmax and isIn.size > 0:

        t = t + dt
        i = i + 1

        # Indexes of cells where each particle is located.
        indx = (xp[isIn]/dx).astype(int)
        indy = (yp[isIn]/dy).astype(int)

        # I thought this was faster
        #indx = np.int_(xp[isIn]/dx)
        #indy = np.int_(yp[isIn]/dy)

        ix = (indy + indx*Ny)

        t_in_incl = update_time_in_incl(t_in_incl, incl_ind, isIn,
                                        indx, indy, t*np.ones(Npart))
        # #Auxiliary array with the numbers of the inclusions
        # #where particles are. Inclusion 0 is the matrix.
        # aux1 = incl_ind[[indy], [indx]].toarray()[0]

        # #index of particles inside an inclusions
        # parts_in_incl = isIn[np.where(aux1 > 0)].astype(int)

        # #index of inclusions where particles are
        # # -1 because zero based convention of arrays...
        # incl = aux1[aux1 > 0].astype(int) - 1

        # #Update of time spent by particles in each inclusion
        # for part, inc in zip(parts_in_incl, incl):
        #     try:
        #         t_in_incl[inc][part] = t_in_incl[inc][part] + dt
        #     except:
        #         t_in_incl[inc][part] = dt

        xp[isIn] = xp[isIn] + (ux[ix] + Ax[ix]*(xp[isIn] - x1[indx]))*dt
        yp[isIn] = yp[isIn] + (uy[ix] + Ay[ix]*(yp[isIn] - y1[indy]))*dt

        #print ["{0:0.19f}".format(i) for i in yp]

        xp[xp < 0.] = 0.

        if isPeriodic:
            yp[yp < 0.] = yp[yp < 0.] + Ly
            yp[yp > Ly] = yp[yp > Ly] - Ly
        else:
            #boundary reflection
            yp[yp < 0.] = -yp[yp < 0.]
            yp[yp > Ly] = 2.*Ly - yp[yp > Ly]

        isOut = isIn[np.where(xp[isIn] >= Lx)]
        arrival_times[isOut] = t # Correction TO DO - (xp[isOut] - Lx)/uxp

        isIn = np.where(xp < Lx)[0]

        if i%nwrite == 0:
            print("Time: %f. Particles inside: %e" %(t, isIn.size))

            if plotit:
                figt, axt, lint = plotXY(xp[isIn], yp[isIn], figt, axt, lint)
                axt.set_aspect('equal')
                axt.set_xlim([0., Lx])
                axt.set_ylim([0., Ly])
                plt.title(t)

    print("Time: %f. Particles inside: %e" %(t, np.sum(isIn)))
    print("End of transport.")

    return arrival_times, t_in_incl

#####
def transport_ds(grid, incl_ind, Npart, ux, uy, ds, isPeriodic=False):
    '''Solves the transport of a line of concentration initially at the
       left integrating the particles' path along the streamlines..

       Returns the arrival times of the particles to the right boundary
       and data about the time spent in the inclusions.'''

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    xp = np.zeros(Npart)
    yp = np.arange(Ly/Npart/2.0, Ly, Ly/Npart)
    #qq
    #rad = 0.25231325
    #alpha = np.arange(np.pi/2. + np.pi/Npart/2.0, 3.*np.pi/2., np.pi/Npart)
    #xp = 1.5 + rad*np.cos(alpha)
    #yp  = 0.5 + rad*np.sin(alpha)
    #qq
    dx = np.float(Lx/Nx)
    dy = np.float(Ly/Ny)

    Ax = ((ux[:, 1:Nx + 1] - ux[:, 0:Nx])/dx).flatten(order='F')
    Ay = ((uy[1:Ny + 1, :] - uy[0:Ny, :])/dy).flatten(order='F')

    ux = ux[:, 0:Nx].flatten(order='F')
    uy = uy[0:Ny, :].flatten(order='F')

    x1 = np.arange(0., Lx + dx, dx) #faces' coordinates
    y1 = np.arange(0., Ly + dy, dy)

    arrival_times = np.zeros(Npart)

    i = 0

    #number of inclusions
    num_incl = incl_ind.max().astype(int)


    # Time of each particle in each inclusion
    # It contains nincl dictionaries.
    # Each dictionary inclusion contain temporarily the
    # times at which the particles enters and leaves the inclusion.
    # At the end of the simulation only the time spent is stored.
    # Example:
    #
    #   t_in_incl[1] --> times for particles that visited inclusion 1.
    #   t_in_incl[1] = {1: [0.1, 3.4], 5: [1.2, 7.9]}
    #
    #   This means particle 1 entered at time 0.1 and left at time 3.4
    #   and particle 3 entered at 1.2 and left at 7.9
    #
    #   At the end of the simulation we will have
    #
    #   t_in_incl[1] = {1: 3.3, 5: 6.7}
    #
    t_in_incl = []

    for i in range(num_incl):
        t_in_incl.append({})

    isIn = np.where(xp < Lx)[0]

    while  isIn.size > 0:

        i = i + 1

        # Indexes of cells where each particle is located.
        indx = (xp[isIn]/dx).astype(int)
        indy = (yp[isIn]/dy).astype(int)

        ix = (indy + indx*Ny)

        t_in_incl = update_time_in_incl(t_in_incl, incl_ind, isIn,
                                        indx, indy, arrival_times)

        uxp = (ux[ix] + Ax[ix]*(xp[isIn] - x1[indx]))
        uyp = (uy[ix] + Ay[ix]*(yp[isIn] - y1[indy]))

        vp = np.sqrt(uxp*uxp + uyp*uyp)

        xp[isIn] = xp[isIn] + ds*uxp/vp
        yp[isIn] = yp[isIn] + ds*uyp/vp

        arrival_times[isIn] = arrival_times[isIn] + ds/vp

        out = np.where(xp[isIn] - Lx >= 0.)
        arrival_times[out] = arrival_times[out] - (xp[out] - Lx)/uxp[out]

        xp[xp < 0.] = 0.

        if isPeriodic:
            yp[yp < 0.] = yp[yp < 0.] + Ly
            yp[yp > Ly] = yp[yp > Ly] - Ly
        else:
            #boundary reflection
            yp[yp < 0.] = -yp[yp < 0.]
            yp[yp > Ly] = 2.*Ly - yp[yp > Ly]

        if i%1000 == 0:
            print("Last particle at: %e" %(np.min(xp)))

        t_in_incl = update_time_in_incl(t_in_incl, incl_ind, isIn,
                                        indx, indy, arrival_times)
        isIn = np.where(xp < Lx)[0]



    print("Particles inside: %e" %(np.sum(isIn)))
    print("End of transport.")

    return  arrival_times, t_in_incl

####
def plotXY(x, y, fig=None, ax=None, lin=None, logx=False, logy=False,
           allowClose=False):
    '''Function to do Y vs X plots. '''

    if fig is None or ax is None:
        fig = plt.figure()
        ax = fig.gca()
    else:
        plt.sca(ax)

    if lin is None:
        lin, = plt.plot(x, y, 'g.')
        plt.ion()
        plt.show()
    else:
        lin.set_xdata(x)
        lin.set_ydata(y)
        fig.canvas.draw()

    if logy:
        ax.set_yscale("log", nonposy='mask')

    if logx:
        ax.set_xscale("log", nonposy='mask')

    if allowClose:
        input("Dale enter y cierro...")
        plt.close()
        fig = None
        ax = None
        lin = None
    return fig, ax, lin
####
def plot2D(grid, C, fig=None, ax=None, title=None, cmap='coolwarm',
           allowClose=False):
    '''Function to do 2 dimensional plots of cell centerd or face centered
       varibles.'''

    if fig is None or ax is None:
        fig = plt.figure()
        ax = fig.gca()

    #Create X and Y meshgrid
    Lx, Ly, Nx, Ny = unpack_grid(grid)
    dx = Lx/Nx
    dy = Ly/Ny

    # xx, yy need to be +1 the shape of C because
    # pcolormesh needs the quadrilaterals.
    # Otherwise the last column is ignored.
    # See: matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.pcolor
    cny, cnx = C.shape

    if cnx > Nx:      # x face centered

        x1 = np.arange(-dx/2., Lx + dx, dx)
        y1 = np.arange(0., Ly + dy/2., dy)

    elif cny > Ny: # y face centered

        x1 = np.arange(0., Lx + dx/2., dx)
        y1 = np.arange(-dy/2., Ly + dy, dy)

    else:              #cell centered

        x1 = np.arange(0., Lx + dx/2., dx)
        y1 = np.arange(0., Ly + dy/2., dy)

    xx, yy = np.meshgrid(x1, y1)

    plt.sca(ax)
    plt.ion()

    mesh = ax.pcolormesh(xx, yy, C, cmap=cmap)

    #plt.axis('equal')
    #plt.axis('tight')
    plt.axis('scaled')

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    cb = plt.colorbar(mesh, cax=cax)

    if title is not None:
        plt.title(title)

    plt.show()

    if allowClose:
        input("Dale enter y cierro...")
        plt.close()
        fig = None
        ax = None

    return fig, ax, cb
################
def number_of_grains(nix, niy, pack):
    """Computes number of grains according to the packing"""

    if pack == 'sqr':
        ngrains = nix*niy
    elif pack == 'tri':
        if nix%2 == 0:
            ngrains = (nix//2)*(niy - 1) + (nix//2)*niy
        else:
            ngrains = (nix//2)*(niy - 1) + (nix//2 + 1)*(niy)
    elif pack == 'rnd':
        ngrains = nix*niy

    return ngrains
################
def load_data(filename):
    """ Loads the data in the given file.
        The file can be a text file (.dat) or
        a pickled file (.plk)
    """

    import os.path as ospath

    _, fileending = ospath.splitext(filename)

    if fileending == '.dat':
        data = np.loadtxt(filename)
        Npart = len(data)
        arrival_times = data
        return  Npart, arrival_times # Npart, t_in_incl if imported from plk

    elif fileending == '.plk':
        with open(filename, 'rb') as ff:
            data = pickle.load(ff)
            Npart = data[0]
            t_in_incl = data[1]
            arrival_times = data[2]
            return  Npart, t_in_incl, arrival_times

################
def time_per_inclusion(t_in_incl, Npart, saveit=False, filename=None):
    """ Given the dictionary whith the time at which each particle entered
        and exited each inclusion, returns the time each particle spent
        in each inclusion in a matrix format suitable to calculate
        histograms.
        It also returns all times in a single array.
        Optionally, the data is saved as a two text files
        incl-times.dat and trap-times.dat).
    """

    #First the total time each particle spent in each inclusion is computed.
    tot_t_in_incl = total_time_in_incl(t_in_incl)

    #dictionary with the time particles spent in each inclusion.
    incl_times = {}
    trapped_part = {}
    i = 0
    for incl in tot_t_in_incl:
        vals = list(incl.values())
        if len(vals)>0:
            incl_times[i] = np.concatenate(vals)
            trapped_part[i] = list(incl.keys())
            i = i + 1
        else:
            incl_times[i] = 0
    trapped_part = flatten_list(list(trapped_part.values()))
    trapped_part = np.unique(np.array(trapped_part))
    num_free_part  = Npart - trapped_part.shape[0]

    trap_times =  np.concatenate(np.array(list(incl_times.values())))
    # adds as many zeros as free particles
    trap_times =  np.concatenate((trap_times, np.zeros(num_free_part)))

    if saveit:

        if filename is None:
            fname = 'incl-times.dat'
        else:
            fname = filename + '-incl-times.dat'

        from itertools import zip_longest

        import csv
        with open(fname, 'w') as ff:
            writer = csv.writer(ff, delimiter=' ')
            # I whished I knew why this works...
            for values in zip_longest(*list(incl_times.values())):
                writer.writerow(np.asarray(values))

        if filename is None:
            fname = 'trap-times.dat'
        else:
            fname = filename + '-trap-times.dat'

        np.savetxt(fname, trap_times)

    return incl_times, trap_times

################
def inclusions_histograms(incl_times, showfig=True, savefig=False,
                          savedata=False, fname='', figformat='pdf',
                          bins='auto'):
    """Plots and saves the histogram for all inclusions."""

    n_incl = len(incl_times)
    i = 0
    for incl in incl_times:

        times = incl_times[incl].flatten()

        title = 'inclusion ' + str(i + 1) + '/' + str(n_incl)
        figname = fname + '-incl-hist-' + str(i) + '.pdf'
        plot_hist(times, title=title, bins=bins,
                  showfig=showfig, savefig=savefig, savedata=savedata,
                  figname=figname, figformat=figformat)
        i = i + 1
    return True

################
def plot_hist(data, title='', bins='auto', density=True, showfig=True,
              savefig=False, savedata=False, figname='zz', figformat='pdf'):
    '''Plots the histogram of the data.'''

    vals, edges = np.histogram(data, bins=bins, density=density)
    if showfig:
        _, _, _ = plotXY((edges[:-1] + edges[1:])/2., vals,
                         allowClose= not savefig)
        if savefig:
            save_fig(xlabel='time', ylabel='freq', title='title',
                     figname=figname, figformat=figformat)

    if savedata:
        filename = figname + '.dat'
        np.savetxt(filename, np.matrix([edges[:-1],edges[1:], vals]).transpose())

    return True
################
def postprocess_from_file(fname, savedata=True, savefig=False,
                          showfig=False, figformat='pdf',
                          bins='auto',dofullpostp=False):
    """Post process from plk file. Computes the histograms for
       particles and inclusions. Saves data and/or figures.
    """

    Npart, t_in_incl, arrival_times = load_data(fname + '.plk')

    postprocess(Npart, t_in_incl, arrival_times, fname=fname,
                savedata=savedata, savefig=savefig,
                showfig=showfig, figformat=figformat,
                bins=bins, dofullpostp=dofullpostp)
    return True

################
def postprocess(Npart, t_in_incl, arrival_times, fname='',
                savedata=True, savefig=False,
                showfig=False, figformat='pdf',
                bins='auto', dofullpostp=False):
    """Post process simulation data.
    """
    _, _ = compute_cbtc(arrival_times, saveit=savedata,
                        showfig=showfig, savefig=savefig,
                        filename=fname)

    t_mobile, t_immobile = mobile_immobile_time(t_in_incl, arrival_times,
                                          filename=fname, saveit=savedata)
    figname = fname + '-immob-hist'
    plot_hist(t_immobile, title='', bins='auto',
              showfig=showfig, savefig=savefig,
              savedata=savedata, figname=figname)

    figname = fname + '-mob-hist'
    plot_hist(t_mobile, title='', bins='auto',
              showfig=showfig, savefig=savefig,
              savedata=savedata, figname=figname)

    incl_times, trap_times = time_per_inclusion(t_in_incl, Npart,
                                       saveit=savedata, filename=fname)

    figname = fname + '-trap-dist'
    plot_hist(trap_times, title='', bins='auto',
              showfig=showfig, savefig=savefig,
              savedata=savedata, figname=figname)

   # TO DO: Verify.
   # _, _ = incl_per_time(t_in_incl, plotit=showfig,
   #                          saveit=savedata, filename=fname)

    _, _, _, _ = free_trapped_arrival(arrival_times, t_immobile,
                                         saveit=savedata, filename=fname)

    incl_per_part = inclusion_per_particle(t_in_incl, Npart, saveit=savedata,
                                           filename=fname)

    if dofullpostp:
    #particle histogram
        plot_hist(trap_times, title=fname + ' particles', bins=bins,
              showfig=showfig, savefig=savepartfig, savedata=savedata,
              figname=fname + '-part-hist', figformat=figformat)


        inclusions_histograms(incl_times, showfig=showfig, savefig=saveinclfig,
                              savedata=False, fname=fname,
                              figformat=figformat, bins=bins)

    return True
################
def postprocess_all(savedata=True, savefig=False,
                    showfig=False, figformat='pdf',
                    bins='auto',dofullpostp=False):

    """ Post process al the cases in a folder."""

    import os as os

    files = os.listdir()
    for file in files:
        if file.endswith('plk'):
            fname = os.path.splitext(file)[0]
            print(fname + '...')
            postprocess_from_file(fname, savedata=savedata, savefig=savefig,
                          showfig=showfig, figformat='pdf',
                                  bins='auto', dofullpostp=dofullpostp)
    return True
################
def stream_function(grid, kperm, isPeriodic=False, plotPsi=False):
    '''Compute the stream function.
    The stream function is prescribed at the boundaries so that
    It is equivalent to prescribed flow on the left and right
    boundaries and no flow on top and bottom boundaries.'''

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    dx = Lx/(Nx-1)
    dy = Ly/(Ny-1)
    Np = Nx*Ny

    D1x, D2x, D1y, D2y = fd_mats(Nx, Ny, dx, dy, isPeriodic=isPeriodic)

    Y = np.log(kperm).reshape(Np, order='F')
    #Kmat = (D1x.multiply((D1x*Y).reshape(Np, 1)) +
    #        D1y.multiply((D1y*Y).reshape(Np, 1)))

    Amat = (D2y + D2x - (D1x.multiply((D1x*Y).reshape(Np, 1)) +
                         D1y.multiply((D1y*Y).reshape(Np, 1)))).tolil()

    RHS = np.zeros(Np)
    #BC
    #
    #Top boundary
    Amat[0:Np:Ny, :] = 0.0
    idx = np.arange(0, Np, Ny)
    Amat[idx, idx] = 1.0
    RHS[0:Np:Ny] = Ly
    #
    #Bottom boundary
    Amat[Ny-1:Np:Ny, :] = 0.0
    idx = np.arange(Ny-1, Np, Ny)
    Amat[idx, idx] = 1.0
    RHS[Ny-1:Np:Ny] = 0.0
    #
    if isPeriodic:
        RHS[0:Np:Ny] = RHS[0:Np:Ny] - Ly/2.0
        RHS[Ny-1:Np:Ny] = RHS[Ny-1:Np:Ny] - Ly/2.0

    #Left boundary
    Amat[0:Ny, :] = 0.0
    idx = np.arange(0, Ny, 1)
    Amat[idx, idx] = 1.0
    RHS[0:Ny] = np.linspace(0., Ly, Ny)[::-1]
    if isPeriodic:
        RHS[0:Ny] = np.linspace(0., Ly, Ny)[::-1] - Ly/2.0
    #
    #Right boundary
    Amat[Np-Ny:Np, :] = 0.0
    idx = np.arange(Np-Ny, Np, 1)
    Amat[idx, idx] = 1.0
    RHS[Np-Ny:Np] = RHS[0:Ny]

    psi = lgsp.spsolve(Amat.tocsr(), RHS).reshape(Ny, Nx, order='F')
    if plotPsi:
        fig, ax, cb = plot2D(grid, kperm, title='K', allowClose=False)
        cb.remove()
        x1 = np.arange(0.0, Lx+dx/2., dx)
        y1 = np.arange(0.0, Ly+dy/2., dy)
        xx, yy = np.meshgrid(x1, y1)
        ax.contour(xx, yy, np.abs(psi), 51, linewidths=1.0, colors='y')
        plt.show()
        input("Dale enter y cierro...")
        plt.close()
    return psi
################
def fd_mats(Nx, Ny, dx, dy, isPeriodic=False):
    '''Computes finite differeces matrices.'''
    Np = Nx*Ny
    dx2 = dx*dx
    dy2 = dy*dy

    # First derivatives in Y
    Dp = np.zeros(Np)
    Dup = np.ones(Np)/(2.0*dy)
    Ddw = -np.ones(Np)/(2.0*dy)
    DperUp = np.zeros(Np)
    DperDw = np.zeros(Np)
    #
    #Top boundary
    Ddw[Ny-1:Np:Ny] = 0.0 #diagonal inferior
    if isPeriodic:
        DperUp[Ny-1:Np:Ny] = -1.0/(2.0*dy)
    else:
        Dp[0:Np:Ny] = -1.0/dy #diagonal
        Dup[1:Np:Ny] = 1.0/dy #diagonal superior
    #
    #Bottom bounday
    Dup[Ny:Np:Ny] = 0.0
    if isPeriodic:
        DperDw[0:Np:Ny] = 1.0/(2.0*dy)
    else:
        Dp[Ny-1:Np:Ny] = 1.0/dy #diagonal
        Ddw[Ny-2:Np:Ny] = -1.0/dy #diagonal inferior
    #
    D1y = sp.spdiags([DperDw, Ddw, Dp, Dup, DperUp],
                     [-Ny+1, -1, 0, 1, Ny-1], Np, Np,
                     format='csr')
    #
    #
    # First derivatives in X
    Dp = np.zeros(Np)
    Dup = np.ones(Np)/(2.0*dx)
    Ddw = -np.ones(Np)/(2.0*dx)
    #
    #Left boundary
    Dp[0:Ny] = -1.0/dx #diagonal
    Dup[Ny:2*Ny] = 1.0/dx #diagonal superior
    #
    # Right boundary
    Dp[Np-Ny:Np] = 1.0/dx #diagonal
    Ddw[Np-2*Ny:Np] = -1.0/dx #diagonal inferior
    #
    D1x = sp.spdiags([Ddw, Dp, Dup], [-Ny, 0, Ny], Np, Np, format='csr')

    #Second derivative in Y
    Dp = -2.0*np.ones(Np)/dy2
    Ddw = np.ones(Np)/dy2
    Dup = np.ones(Np)/dy2
    Dup2 = np.zeros(Np) #segunda diagonal superior
    Ddw2 = np.zeros(Np) #segunda diagonal inferior
    DperUp = np.zeros(Np)
    DperDw = np.zeros(Np)
    #
    # Top boundary
    if isPeriodic:
        DperUp[Ny-1:Np:Ny] = 1.0/(dy2)
        Ddw[Ny-1:Np:Ny] = 0. # diagonal inferior
    else:
        Dp[0:Np:Ny] = 1.0/dy2 #diagonal
        Ddw[Ny-1:Np:Ny] = 0. # diagonal inferior
        Dup[1:Np:Ny] = -2.0/dy2 #diagonal superior
        Dup2[2:Np:Ny] = 1.0/dy2
    #
    # Bottom boundary
    if isPeriodic:
        DperDw[0:Np:Ny] = 1.0/(dy2)
        Dup[Ny:Np:Ny] = 0.0 #Diagonal superior
    else:
        Dp[Ny-1:Np:Ny] = 1.0/dy2 #diagonal
        Ddw[Ny-2:Np:Ny] = -2.0/dy2 #diagonal inferior
        Ddw2[Ny-3:Np:Ny] = 1.0/dy2
        Dup[Ny:Np:Ny] = 0.0 #Diagonal superior

    D2y = sp.spdiags([Ddw2, Ddw, Dp, Dup, Dup2],
                     [-2, -1, 0, 1, 2],
                     Np, Np, format='csr')
    if isPeriodic:
        D2y = D2y + sp.spdiags([DperDw, DperUp],
                               [-Ny + 1, Ny - 1],
                               Np, Np, format='csr')

    #Second derivative in X
    Dp = -2.0*np.ones(Np)/dx2
    Ddw = np.ones(Np)/dx2
    Dup = np.ones(Np)/dx2
    #
    # Left boundary
    Dp[0:Ny] = 1.0/dx2 #diagonal
    Dup[Ny:2*Ny] = -2.0/dx2 #diagonal superior
    Dup2 = np.zeros(Np) #segunda diagonal superior
    Dup2[2*Ny:3*Ny] = 1.0/dx2
    #
    # Right boundary
    Dp[Np-Ny:Np] = 1.0/dx2 #diagonal
    Ddw[Np-2*Ny:Np] = -2.0/dx2 #diagonal inferior
    Ddw2 = np.zeros(Np) #segunda diagonal inferior
    Ddw2[Np-3*Ny:Np-2*Ny] = 1.0/dx2

    D2x = sp.spdiags([Ddw2, Ddw, Dp, Dup, Dup2],
                     [-2*Ny, -Ny, 0, Ny, 2*Ny],
                     Np, Np, format='csr')

    return D1x, D2x, D1y, D2y
################
def update_time_in_incl(t_in_incl, incl_ind, isIn, indx, indy, time):
    """ Given the particles inside the domain (isIn), the indexes of
        the cells where the particles are (indx, indy), the indexes
        of the cells inside incluisions (incl_ind), and the current time
        (per particle if integrating along streamlines) (time), updates
        the time the particle entered and/or exited each inclusion.
    """
    #Auxiliary array with the numbers of the inclusions
    #where particles are. Inclusion 0 is the matrix.
    aux1 = incl_ind[[indy], [indx]].toarray()[0]

    #index of particles inside an inclusion
    parts_in_incl = isIn[np.where(aux1 > 0)].astype(int)

    #index of inclusions where particles are
    # -1 because zero based convention of arrays...
    incl = aux1[aux1 > 0].astype(int) - 1

    #Update of time spent by particles in each inclusion
    for part, inc in zip(parts_in_incl, incl):
        try:
            t_in_incl[inc][part][1] = time[part]
        except:
            t_in_incl[inc][part] = [time[part], 0.0]

    return t_in_incl
################
def total_time_in_incl(t_in_incl):
    """ Given the dictionary whith the time at which each particle entered
        and exited each inclusion, returns the total time spent for each
        particle in each inclusion.
    """
    # total time of particles in inclusions
    num_incl = len(t_in_incl)
    incl = np.arange(0, num_incl, 1)
    tot_t_in_incl = t_in_incl.copy()

    for dic1, incl in zip(tot_t_in_incl, incl):
        tot_t_in_incl[incl] = {k:np.diff(v) for k, v in dic1.items()}

    return tot_t_in_incl
################
def compute_cbtc(arrival_times, bins=None, saveit=False,
                 logx=False, logy=False, showfig=False,
                 savefig=False, filename=None):
    ''' Cummulative breakthrough curve from arrival times.'''
    Npart = arrival_times.shape[0]
    if bins is None:
        cbtc_time = np.sort(arrival_times)
        cbtc = 1. - np.cumsum(np.ones(Npart))/Npart
    else:
        vals, edges = np.histogram(arrival_times, bins=bins, density=False)
        cbtc_time = (edges[:-1] + edges[1:])/2.
        cbtc = 1. - np.cumsum(vals)/Npart

    if saveit:
        fname = 'cbtc.dat'
        if filename is not None:
            fname = filename + '-' + fname

        np.savetxt(fname, np.matrix([cbtc_time, cbtc]).transpose())
    if showfig:
        _, _, _ = plotXY(cbtc_time, cbtc, logx=logx, logy=logy, allowClose=True)

        if savefig:
            save_fig(xlabel='time', ylabel='cbtc', title='',
                     figname=fname, fogformat='pdf')

    return cbtc_time, cbtc

################
def mobile_immobile_time(t_in_incl, arrival_times, filename=None, saveit=False):

    #adds up the time spent in each inclusion

    t_immobile = np.zeros(arrival_times.shape[0])
    for incl in t_in_incl:
        for part in incl:
            t_immobile[part] = t_immobile[part] + np.diff(incl[part])

    t_mobile = arrival_times - t_immobile

    if saveit:
        fname = 'mob.dat'
        if filename is not None:
            fname = filename + '-' + fname

        np.savetxt(fname, t_mobile)

        fname = 'immob.dat'
        if filename is not None:
            fname = filename + '-' + fname

        np.savetxt(fname, t_immobile)

    return t_mobile, t_immobile
################
def perm_matrix(grid, circles, Kfactor):
    """Creates the permeability matrix and indexes of inclusions
        given a set of circles."""

    Lx, Ly, Nx, Ny = unpack_grid(grid)

    kperm = np.ones([Ny, Nx])
    incl_ind = np.zeros([Ny, Nx])
    dx = Lx/Nx
    dy = Ly/Ny
    x1 = np.arange(dx/2.0, Lx, dx) #cell centers' coordinates
    y1 = np.arange(dy/2.0, Ly, dy)
    xx, yy = np.meshgrid(x1, y1)

    i = 0

    for circ in circles:
        i = i + 1
        x0 = circ['x']
        y0 = circ['y']
        r = circ['r']
        mask = ((xx - x0)**2.0 + (yy - y0)**2.0) < r**2.0
        kperm[mask] = Kfactor
        incl_ind[mask] = i

    return kperm, sp.csr_matrix(incl_ind)
################
def load_perm(fname):
    """ Loads the permeability distribution in the given plk file."""

    with open(fname + '-perm.plk', 'rb') as ff:
        data = pickle.load(ff)
        grid = data[0]
        circles = data[1]
        Kfactor = data[2]

    return grid, circles, Kfactor
################
def incl_per_time(t_in_incl, plotit=False, saveit=False, filename=None):
    """ Given the dictionary whith the time at which each particle entered
        and exited each inclusion, returns the total number of inclusions
        that contain at least one particle at a given time.
    """

    # total time of particles in inclusions
    num_incl = len(t_in_incl)
    incl_indx = np.arange(0, num_incl, 1)

    # We build an array with all residence times
    # (entrance and exit of each particle in each visited inclusion).
    residence_times = [list(d.values()) for d in t_in_incl]

    residence_times = np.asarray(flatten_list(residence_times))

    #Latest timeat which an inclusion was occupied.
    tmax = residence_times.max()

    # Then we divide the time interval in 1000 parts.
    times = np.arange(0.0, tmax + tmax/10, tmax/10)
    occ_incl = np.zeros(times.shape[0])
    i = 0

    #For each time we check how many inclusions are occupied.
    for t in times:
        a1 = np.array(residence_times[:, 0] < t)
        a2 = np.array(residence_times[:, 1] > t)
        occ_incl[i] = np.sum(a1*a2)
        #isOccupied = np.where( (residence_times[:, 0] < t)
        #                     & (residence_times[:, 1] > t))
        #occ_incl[i] = np.sum(np.asarray(isOccupied)>0)
        i = i + 1

    if plotit:
        plotXY(times, occ_incl, allowClose=True)

    if saveit:
        fname = 'occ-incl.dat'

        if filename is not None:
            fname = filename + '-' + fname

        np.savetxt(fname, np.matrix([times, occ_incl]).transpose())

    return times, occ_incl
################
def plot_perm_from_file(fname):
    '''Load permeability data from plk file and plots it.'''
    #TO DO : check that file exists.
    grid, circles, Kfactor = load_perm(fname)
    kperm, _ = perm_matrix(grid, circles, Kfactor)
    plot2D(grid, kperm, title='K', allowClose=True)
################
def free_trapped_arrival(arrival_times, t_immobile, bins=None,
                         saveit=False, logx=False, logy=False,
                         showfig=False, savefig=False, filename=None):
    """Given the arrival time and the immobile time of all particles,
        returns the arrival times of particles that visited at least
        one inclusion (trapped) and of those that did not visit any
        inclusion (free).
        Optionally, the data is saved as a text file.

    """
    if filename is None:
        filename = ''
    else:
        filename = filename + '-'

    trapped = t_immobile > 0.

    arrivals_trapped = arrival_times[trapped]
    arrivals_free = arrival_times[~ trapped]

    traptime, trapcbtc = compute_cbtc(arrivals_trapped, bins=bins,
                                      saveit=saveit, logx=logx, logy=logy,
                                      showfig=showfig, savefig=savefig,
                                      filename=filename + 'trap')

    freetime, freecbtc = compute_cbtc(arrivals_free, bins=bins,
                                      saveit=saveit, logx=logx, logy=logy,
                                      showfig=showfig, savefig=savefig,
                                      filename=filename + 'free')


    return traptime, trapcbtc, freetime, freecbtc
################
def inclusion_per_particle(t_in_incl, Npart, saveit=False, showfig=False,
                           savefig=False, filename=None):
    """ Given the dictionary whith the time at which each particle entered
        and exited each inclusions, returns the number of inclusions visited
        by each particle.
        Optionally, the data is saved as a text file.
    """
    incl_per_part = np.zeros(Npart)
    for incl in t_in_incl:
        incl_per_part[list(incl.keys())] = incl_per_part[list(incl.keys())] + 1

    if saveit:
        fname = 'visited-incl.dat'
        if filename is not None:
            fname = filename + '-' + fname

        np.savetxt(fname, incl_per_part)

        fname = 'trap-events'
        if filename is not None:
            fname = filename + '-' + fname

    if saveit or showfig:
        num_incl = len(t_in_incl)
        bins = np.arange(0, num_incl + 1)
        bins = np.arange(0, np.max(incl_per_part) + 1)
        plot_hist(incl_per_part, title='', bins=bins,
                  showfig=showfig, savefig=savefig, savedata=saveit,
                  figname=fname)

    print("Trapping Events.")
    print("Mean: " + str(np.mean(incl_per_part)))
    print( "Variance: " + str(np.var(incl_per_part)))

    return incl_per_part
################
def save_fig(xlabel='', ylabel='', title='',figname='',figformat='pdf'):
    '''Saves current figure.'''

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)

    if figformat == 'pdf':
        plt.savefig(figname + '.pdf', format='pdf')
    if figformat == 'tikz':
        from matplotlib2tikz import save as tikz_save
        tikz_save(figname + '.tex')

    input("Dale enter y cierro...")
    plt.close()
################
def equivalent_permeability(grid, kperm, isPeriodic=False):
    """Compute the  equivalent permeability of the medium.
    """
    bcc='head'
    ux, _ = flow(grid, 1./kperm, bcc, isPeriodic=isPeriodic, plotHead=False)
    dy = grid['Ly']/grid['Ny']
    gradH = 1./grid['Lx']
    Q = np.sum(ux[:,-1])*dy

    return Q/(grid['Ly']*gradH)
################
def equivalent_permeability_from_file(fname):
    """Compute the equivalent permeability of the medium.
       Data read from perm.plk file.
    """
    grid, circles, Kfactor = load_perm(fname)
    kperm, _ = perm_matrix(grid, circles, Kfactor)

    return equivalent_permeability(grid, kperm)
################
def inclusion_area(grid, circles, Kfactor):
    '''Computes inclusions area.'''

    kperm, _ = perm_matrix(grid, circles, Kfactor)
    dx = grid['Lx']/grid['Nx']
    # We do not take into account the displacement
    radius = circles[0]['r']
    displacement = np.ceil(4.*radius)
    ndisp  = np.int(displacement/dx/2)
    #kperm = kperm[:,ndisp:-ndisp]
    kshape = kperm.shape

    incl_area =  sum(kperm[:,ndisp:-ndisp].flatten()<1.)
    total_area = sum(kperm[:,ndisp:-ndisp].flatten()>0.)

    return incl_area/total_area, kperm
################
def flatten_list(l):
    '''Merge items in list with sublists.
       From
       https://stackoverflow.com/questions/952914/making-a-flat-list-out-of-list-of-lists-in-python
    '''
    return  [item for sublist in l for item in sublist]
################
def average_number_of_inclusions(kperm):
    ''' Computes the average number of inclusions in the vertical and
        horizontal directions'''

    h_aux = np.sum(np.abs(np.diff(kperm, axis=1))>0, axis=1)/2
    v_aux = np.sum(np.abs(np.diff(kperm, axis=0))>0, axis=0)/2

    #mean in the whole domain
    h_avg = np.mean(h_aux)
    v_avg = np.mean(v_aux)

    #mean removing zeros
    h_avg_nonzero = np.nanmean(np.where(h_aux!=0, h_aux, np.nan))
    v_avg_nonzero = np.nanmean(np.where(v_aux!=0, v_aux, np.nan))

    return h_avg, v_avg, h_avg_nonzero, v_avg_nonzero
################
def permeability_data(fname):

    grid, circles, Kfactor = load_perm(fname)

    print('Lx = '+ str(grid['Lx']))
    print('radius = '+ str(circles[0]['r']))
    print('inclusions = '+ str(circles.shape[0]))
    incl_area, kperm = inclusion_area(grid, circles, Kfactor)
    print('inclusion_area = ' + str(incl_area))

    keff = equivalent_permeability(grid, kperm)
    print('Keff = ' + str(keff))

    return grid['Lx'], circles[0]['r'], circles.shape[0], incl_area, keff
################
def get_mesh(grid, centering='cell'):
    """ Returns face centered, x cenered or y centered mesh.
        centering = cell, x, y.
    """

    Lx, Ly, Nx, Ny = unpack_grid(grid)
    dx = Lx/Nx
    dy = Ly/Ny

    if centering == 'x':     # x face centered

        x = np.arange(0., Lx + dx, dx)
        y = np.arange(0., Ly + dy/2., dy)

    elif centering == 'y':      # y face centered

        x = np.arange(0., Lx + dx/2., dx)
        y = np.arange(0., Ly + dy, dy)

    elif centering == 'cell':  #cell centered

        x = np.arange(dx, Lx + dx/2., dx)
        y = np.arange(dy, Ly + dy/2., dy)

    xx, yy = np.meshgrid(x, y)

    return xx, yy, x, y
################
def velocity_distribution(grid, kperm, ux=None, uy=None, incl_ind=None,
                          bins='auto', showfig=False, savefig=False,
                          savedata=False, fname=''):

    """Computes the velocity distribution in the inclusions.
    """

    xx, _, _, _ = get_mesh(grid)
    xmax = np.max(xx[kperm<1.])
    xmin = np.min(xx[kperm<1.])

    if ux is None or uy is None:
        ux, uy = flow(grid, 1./kperm, 'flow', isPeriodic=True, plotHead=False)

    uxm = (ux[:, 0:-1] + ux[:, 1:])/2.
    uym = (uy[0:-1, :] + uy[1:, :])/2.
    vel = np.sqrt(uxm*uxm + uym*uym)

    figname  = fname + '-vel-incl'

    plot_hist(vel[kperm<1.0], title='', bins=bins, density=True,
              showfig=showfig, savefig=savefig, savedata=savedata,
              figname=figname, figformat='pdf')

    figname  = fname + '-vel-mat'

    plot_hist(vel[kperm>0.99], title='', bins=bins, density=True,
              showfig=showfig, savefig=savefig, savedata=savedata,
              figname=figname, figformat='pdf')


    figname  = fname + '-vel-mat-no-buffer'
    mask = (kperm > 0.99) & (xx > xmin) & (xx < xmax)
    plot_hist(vel[mask], title='', bins=bins, density=True,
              showfig=showfig, savefig=savefig, savedata=savedata,
              figname=figname, figformat='pdf')

    figname  = fname + '-vel-all'

    plot_hist(vel, title='', bins=bins, density=True,
              showfig=showfig, savefig=savefig, savedata=savedata,
              figname=figname, figformat='pdf')

    # Statistics by inclusion
    if incl_ind is not None:
        num_incl = incl_ind.max().astype(int)

        vmean = np.zeros(num_incl)

        incl_ind = incl_ind.todense().astype(int)
        for i in range(num_incl):
            figname  = fname + '-vel-' + str(i)
            plot_hist(vel[incl_ind==(i+1)], title='', bins=bins, density=True,
                      showfig=showfig, savefig=savefig, savedata=savedata,
                      figname=figname, figformat='pdf')

            vmean[i] = np.mean(vel[incl_ind==i])

        figname = fname + '-vel-mean'
        plot_hist(vmean, title='', bins=bins, density=True,
                  showfig=showfig, savefig=savefig, savedata=savedata,
                  figname=figname, figformat='pdf')

    return True
################
def velocity_distribution_from_file(fname, folder='.'):
    ''' Loads data and computes the velocity distributions.
    '''
    permfile = folder + '/' + fname

    grid, circles, Kfactor = load_perm(permfile)
    radius = circles[0]['r']
    kperm, incl_ind = perm_matrix(grid, circles, Kfactor)

    _ =  velocity_distribution(grid, kperm, ux=None, uy=None, incl_ind=incl_ind,
                               bins='auto', showfig=False, savefig=False,
                               savedata=True, fname=fname)

    return True
