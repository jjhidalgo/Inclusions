# Inclusions
Solves flow and transport in a rectangular porous media with spherical inclusions with a certain permeability contrasts with respect to the medium.

Inclusions.py: Module with the flow and transport solvers.

test.py: An example

Dependencies: Needs RecPore2D